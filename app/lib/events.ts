type TListener = (e: Event) => void

export class EventEmitter{
  public _: HTMLElement

  constructor(){
    this._ = document.createElement('div')
  }

  public on(type: string, listener: TListener){
    this._.addEventListener(type, listener)
  }

  public off(type: string, listener: TListener){
    this._.removeEventListener(type, listener)
  }

  public emit(type: string){
    this._.dispatchEvent(new Event(type))
  }
}
