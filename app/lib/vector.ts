import * as utils from './utils'

export interface IVec2 {
  x: number
  y: number
}

export class Vec2 implements IVec2 {
  public x: number = 0
  public y: number = 0

  constructor(data: number[] | Vec2 = [0,0]){
    if(Array.isArray(data)){
      [this.x, this.y] = data
      return
    }

    if(data instanceof Vec2){
      this.x = data.x
      this.y = data.y
      return
    }

    throw new Error('Unknown format Vec2')
  }

  public direct(dist: number, angle: number): Vec2{
    let {sin, cos} = Math

    return new Vec2([dist * cos(angle), dist * sin(angle)])
  }

  public rangeX(min: number, max: number, diff: number = 0): number {
    return utils.range(this.x, min, max, diff)
  }

  public rangeY(min: number, max: number, diff: number = 0): number {
    return utils.range(this.y, min, max, diff)
  }

  public move(v: IVec2): Vec2{
    this.moveX(v.x)
    this.moveY(v.y)
    return this
  }

  public moveX(v: number): Vec2{
    this.x += v
    return this
  }

  public moveY(v: number): Vec2{
    this.y += v
    return this
  }

  public get copyVec(): Vec2 {
    return new Vec2(this)
  }

  public get center(): Vec2 {
    return new Vec2([this.x / 2, this.y / 2])
  }
}
