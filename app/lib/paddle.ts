import * as utils from './utils'

import {Vec2} from './vector'

export class Paddle {
  constructor(public position: Vec2, public size: number) {

  }
}
