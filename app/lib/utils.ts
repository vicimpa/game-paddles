export function rand(min: number = 0, max: number = 100): number {
  return Math.floor((Math.random() * max) + min) - min
}

export function radToDeg(rad: number): number {
  return rad / Math.PI * 180
}

export function degToRad(deg: number): number {
  return deg / 180 * Math.PI
}

export function range(value: number, min: number, max: number, diff: number = 0): number{
  if(value + diff >= max)
    return 1
  if(value - diff <= min)
    return -1
  return 0
}
