import * as utils from './utils'

import {Vec2} from './vector'
import {EventEmitter} from './top-events'

import {Scene} from './scene'

export class Ball extends EventEmitter {
  public speed: number
  public radius: number
  public color: string = '#fff'
  public angle: number = utils.degToRad(utils.rand(0, 360))

  constructor(public position: Vec2, radius: number, speed: number = 200) {
    super()

    this.radius = radius
    this.speed = speed
  }

  public setColor(color: string) {
    this.color = color
  }

  public update(scene: Scene) {
    let r = this.radius

    let dist = this.speed * scene.timeDiff
    let movePos = this.position.direct(dist, this.angle)
    let targetPos = this.position.copyVec.move(movePos)

    let xR, yR:number

    if((xR = targetPos.rangeX(0, scene.size.x, r)) !== 0){
      this.emit(`ball`, `${xR < 0 ? 'left' : 'right' }`)
      this.angle = Math.atan2(movePos.y, -movePos.x)
      return this.update(scene)
    }

    if((yR = targetPos.rangeY(0, scene.size.y, r)) !== 0){
      this.emit(`ball`, `${yR < 0 ? 'top' : 'bottom' }`)
      this.angle = Math.atan2(-movePos.y, movePos.x)
      return this.update(scene)
    }

    this.position = targetPos
  }

  public render(scene: Scene) {
    let {x, y} = this.position
    let {context} = scene

    context.beginPath()
    context.fillStyle = this.color
    context.arc(x, y, this.radius, 0, Math.PI*2, false)
    context.closePath()
    context.fill()
  }
}
