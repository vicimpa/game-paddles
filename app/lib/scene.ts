import {Vec2} from './vector'

export interface ISceneObject{
  update(scene: Scene)
  render(scene: Scene)
}

export class Scene{
  public time: number = 0
  public timeDiff: number = 0
  public timeNow: number = Date.now()
  public size: Vec2
  public context: CanvasRenderingContext2D
  public objects: ISceneObject[] = []

  constructor(public canvas: HTMLCanvasElement) {
    this.context = canvas.getContext('2d')
    this.size = new Vec2([canvas.width, canvas.height])

    this.loop()
  }

  public addObject(object: ISceneObject) {
    this.objects.push(object)
  }

  public removeObject(object: ISceneObject) {
    let index = this.objects.indexOf(object)

    if(index !== -1)
      this.objects.splice(index, 1)
  }

  public update(){
    this.timeDiff = (Date.now() - this.timeNow) / 1000.0

    for(let object of this.objects)
      object.update(this)


    this.time += this.timeDiff
    this.timeNow = Date.now()
  }

  public render(){
    this.clear()

    for(let object of this.objects)
      object.render(this)
  }

  public loop(){
    this.update()
    this.render()

    requestAnimationFrame(() => this.loop())
  }

  public clear() {
    let {x: width, y: height} = this.size
    this.context.clearRect(0, 0, width, height)
  }
}
