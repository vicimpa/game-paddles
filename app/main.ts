import * as utils from './lib/utils'

import {Ball} from './lib/ball'
import {Scene} from './lib/scene'

const timeElem = document.querySelector('p b')
const canvas = document.querySelector('canvas')
const scene = new Scene(canvas)

const ball = new Ball(scene.size.center, 5)

scene.addObject(ball)

// ball.on('ball', (...data) => console.log(...data))

canvas.style.background = '#333'
